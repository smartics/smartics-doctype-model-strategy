<?xml version='1.0'?>
<!--

    Copyright 2018-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="swot"
  base-template="standard"
  provide-type="standard-type">
  <resource-bundle>
    <l10n>
      <name plural="SWOT Analyses">SWOT Analysis</name>
      <description>
        Strengths, weaknesses, opportunities, and threats
      </description>
      <about>
        Analyse the current situation by addressing strengths, weaknesses,
        opportunities, and threats for an organization or product to reach the
        desired objectives. The result of this analysis is an assessment of the
        current situation. Based on this information a strategy to reach the
        objectives can be derived.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="SWOT-Analysen">SWOT-Analyse</name>
      <description>
        Stärken, Schwächen, Chancen und Bedrohungen
      </description>
      <about>
        Verwenden Sie eine SWOT-Analyse als Instrument der strategischen
        Planung zur Erreichung eines gesteckten Ziels. Analysieren Sie dabei
        die externen und internen Faktoren in Form der Umwelt (Chancen und
        Bedrohungen) und des Unternehmens oder Produkts (Stärken und Schwächen).
        Das Ergebnis dieser Analyse ist eine Bewertung der aktuellen Situation.
        Ausgehend von diesen Informationen kann dann eine Strategie erarbeitet
        werden, die zur Erlangung des gesteckten Ziels führt.
      </about>
      <type plural="SWOT-Typen">SWOT-Typ</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">swot-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.swot.objectives">
      <resource-bundle>
        <l10n>
          <name>Objectives</name>
          <description>
            Define the objectives you require to reach. Reference to or
            transclude from the strategy document in case the vision is already
            described there.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Ziele</name>
          <description>
            Definieren Sie die Ziele, die Sie erreichen wollen. Referenzieren
            Sie das oder transkludieren Sie aus dem Strategiedokument, falls
            die Vision bereits dort dargelegt ist.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.swot.strengths">
      <resource-bundle>
        <l10n>
          <name>Strengths</name>
          <description>
            Analyze the strengths of your organization that support reaching
            the objectives.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Stärken</name>
          <description>
            Analysieren Sie die Stärken Ihres Unternehmens, die Ihnen dabei
            helfen, das gesteckte Ziel zu erreichen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.swot.weaknesses">
      <resource-bundle>
        <l10n>
          <name>Weaknesses</name>
          <description>
            Analyze the weaknesses of your organization or product (in
            comparison to your competitors) that may prevent reaching the
            objectives.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Schwächen</name>
          <description>
            Analysieren Sie die Schwächen Ihres Unternehmens oder Produkts (im
            Vergleich mit der Konkurrenz), die Ihnen bei der Erreichung des
            gesteckten Ziels im Weg stehen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.swot.opportunities">
      <resource-bundle>
        <l10n>
          <name>Opportunities</name>
          <description>
            Analyze the opportunities the environment provides to reach the
            objectives.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Chancen</name>
          <description>
            Analysieren Sie die Chancen, die sich außerhalb Ihres Unternehmens
            bieten, um das gesteckte Ziel zu erreichen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.swot.threats">
      <resource-bundle>
        <l10n>
          <name>Threats</name>
          <description>
            Analyze the threats outside of your organization that may prevent
            your from reaching the objectives.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Bedrohungen</name>
          <description>
            Analysieren Sie die Bedrohungen außerhalb Ihres Unternehmens, die
            die Erreichung des gesteckte Ziel verhindern können.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.swot.so">
      <resource-bundle>
        <l10n>
          <name>Strengths - Opportunities</name>
          <description>
            Show how your strengths can be used to seize opportunities.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Stärken - Chancen</name>
          <description>
            Zeigen Sie auf, wie Sie Ihre Stärken einsetzen können, um
            identifizierte Chancen zu nutzen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.swot.st">
      <resource-bundle>
        <l10n>
          <name>Strengths - Threats</name>
          <description>
            Show how your strengths can be used to remove a threat or to lessen
            the negative consequences of its impact.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Stärken - Bedrohungen</name>
          <description>
            Zeigen Sie auf, wie Sie Ihre Stärken einsetzen können, um
            eine Bedrohung abzuwenden oder deren Auswirkungen abzumildern.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.swot.wo">
      <resource-bundle>
        <l10n>
          <name>Weaknesses - Opportunities</name>
          <description>
            Show how weaknesses can generate opportunities or can be turned into
            strengths.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Schwächen - Chancen</name>
          <description>
            Zeigen Sie auf, wie Sie aus Ihren Schwächen Chancen kreieren oder
            sie in Stärken verwandeln können.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.swot.wt">
      <resource-bundle>
        <l10n>
          <name>Weaknesses - Threats</name>
          <description>
            Show where your weaknesses are and describe how harm could be
            prevented.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Schwächen - Bedrohungen</name>
          <description>
            Zeigen Sie auf, wo Ihren Schwächen konkret lokalisiert sind und
            wie Sie sich vor möglichen Schaden schützen können.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="strategy" />
    <doctype-ref id="mission-statement" />
    <doctype-ref id="vision-statement" />
  </related-doctypes>
</doctype>
